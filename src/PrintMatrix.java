import java.util.Arrays;

public class PrintMatrix {
	
	
	public int[][] generateZeroToOne(int number){
		int[][] emptyArray = new int[number][number];
		int limit = emptyArray.length -1;
		
		for( int i = 0; i<= emptyArray.length -1; i++) {
			emptyArray[i][i] = 1;
			emptyArray[i][limit - i] = 1;
			emptyArray[limit][i] = 1;
			//System.out.println(Arrays.toString(emptyArray[i]));	
		};
		printArrays(emptyArray,number);
		return emptyArray;
		
	}
	
	public int[][] generateSpiral (int number) {
		int[][] templateArray = new int[number][number];
		int limit = templateArray.length -1;
		int first = 0;
		int counter = 1;
		
		while(counter <= (number * number)) {
			//This for fills the fist array left to right 
			for (int i = first; i <= limit; i++ ) {
				templateArray[first][i] = counter++;
			}
			
			//this array fills the right side of the matrix
			for(int i = first +1; i<= limit; i++) {
				templateArray[i][limit] = counter ++;
			}
			
			//this array fills the last array right to left
			for(int i = limit-1; i >=first; i--) {
				templateArray[limit][i]= counter ++;
			}
			
			//this array fills the left side of the matrix
			for(int i = limit-1; i>= first+1; i--) {
				templateArray[i][first]= counter++;
			}
			
			first = first+1;
			limit = limit-1;
			
		}
		printArrays(templateArray, number);
		return templateArray;
		
	}
	
	public void printArrays(int[][] templateArray, int number){
		for(int i = 0; i < number; i++) {
			System.out.println();
			for(int j = 0; j< number; j++) {
				System.out.print(templateArray[i][j]+"\t");
			}
		}
	}

}
