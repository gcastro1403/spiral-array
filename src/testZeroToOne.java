import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testZeroToOne {
	PrintMatrix printMatrix = new PrintMatrix();

	@Test
	void testGenerateZeroToOne() {
		int[][] arr = {{1, 0, 1}, {0, 1, 0}, {1, 1, 1}};
		int[][] expectArray = printMatrix.generateZeroToOne(3);
		assertArrayEquals(arr,expectArray);
		
	}
	
	@Test
	void testGenerateZeroToOneFalse() {
		int[][] arr = {{0, 0, 0}, {0, 0, 0}, {1, 0, 1}};
		int[][] expectArray = printMatrix.generateZeroToOne(3);
		assertNotEquals(arr,expectArray);
	}
	
	@Test
	void testGenerateSpiral() {
		int[][] arr = {{1, 2, 3}, {8, 9, 4}, {7, 6, 5}};
		int [][] expectedArray = printMatrix.generateSpiral(3);
		assertArrayEquals(arr,expectedArray);
	}
	
	@Test
	void testGenerateSpiralFalse() {
		int[][] arr = {{1, 0, 0}, {0, 0, 0}, {8, 6, 5}};
		int [][] expectedArray = printMatrix.generateSpiral(3);
		assertNotEquals(arr,expectedArray);
	}
}
